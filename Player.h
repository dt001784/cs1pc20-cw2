// Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"
#include "Room.h"
#include "Inventory.h" // Include the Inventory class header
#include "Boss.h"
#include "Item.h"

class Player : public Character {
private:
    Room* location;
    Inventory inventory; // Add an Inventory member

public:
    Player(const std::string& name, int health);
    void SetLocation(Room* room);
    Room* GetLocation() const;
    Inventory& GetInventory(); // Add a method to get the player's inventory
    int GetHealth() const; // Declaration of GetHealth
    bool HasItem(const std::string& itemName) const; // Declaration of HasItem method
    void IncreaseHealth(int amount); // Declaration of IncreaseHealth method
    void TakeDamage(int damage);
    void UseItem(const std::string& itemName, Boss& boss); // Add this method for using items in boss fights
};

#endif // PLAYER_H
