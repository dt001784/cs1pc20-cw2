#include <iostream>
#include "Room.h"
#include "Player.h"
#include "Item.h"
#include "Boss.h"
#include <algorithm> // Include for erase function
#include <limits>
#include <cstdlib> // Include for rand()
bool bossPresent; // Define bossPresent only once

int main() {
    // Create Rooms
    Room startRoom("Enchanted Forest");
    Room northRoom("Misty Mountains");
    Room southRoom("Cavern of Whispers");
    Room eastRoom("Haunted Mansion");
    Room westRoom("Abandoned Mines");
    Room northeastRoom("Shimmering Lake");
    Room northwestRoom("Tower of Wisdom");
    Room southeastRoom("Forgotten Ruins");
    Room southwestRoom("Luminous Garden");

    // Define exits between rooms
    startRoom.AddExit("north", &northRoom);
    startRoom.AddExit("south", &southRoom);
    startRoom.AddExit("east", &eastRoom);
    startRoom.AddExit("west", &westRoom);

    northRoom.AddExit("south", &startRoom);
    southRoom.AddExit("north", &startRoom);
    eastRoom.AddExit("west", &startRoom);
    westRoom.AddExit("east", &startRoom);

    northRoom.AddExit("northeast", &northeastRoom);
    northRoom.AddExit("northwest", &northwestRoom);
    southRoom.AddExit("southeast", &southeastRoom);
    southRoom.AddExit("southwest", &southwestRoom);
    eastRoom.AddExit("northeast", &northeastRoom);
    eastRoom.AddExit("southeast", &southeastRoom);
    westRoom.AddExit("northwest", &northwestRoom);
    westRoom.AddExit("southwest", &southwestRoom);

    northeastRoom.AddExit("southwest", &northRoom);
    northwestRoom.AddExit("southeast", &northRoom);
    southeastRoom.AddExit("northwest", &southRoom);
    southwestRoom.AddExit("northeast", &southRoom);

    Boss boss1("Dragon", 200, 20);  // Boss in Enchanted Forest
    Boss boss2("Giant Spider", 250, 25); // Boss in Misty Mountains
    Boss boss3("Dark Sorcerer", 300, 30); // Boss in Cavern of Whispers
    Boss boss4("Undead Knight", 350, 35); // Boss in Haunted Mansion
    Boss boss5("Ancient Wyrm", 400, 40); // Boss in Abandoned Mines

    // Place bosses in corresponding rooms
    startRoom.SetBoss(&boss1);
    northRoom.SetBoss(&boss2);
    southRoom.SetBoss(&boss3);
    eastRoom.SetBoss(&boss4);
    westRoom.SetBoss(&boss5);

    // Create Items
    Item key("Key", "A shimmering key that glows softly. Find the chest near northwest!");
    Item sword("Sword", "A legendary sword, said to have been wielded by heroes of old. Your damage to bosses has increased to 80!");
    Item chest("Chest", "A mysterious chest waiting to be unlocked.");
    Item juggernog("Juggernog", "A powerful elixir that increases health by 100.");

    // Add items to rooms
    startRoom.AddItem(key);
    southeastRoom.AddItem(sword);
    northwestRoom.AddItem(chest); // Add the chest in the northwestRoom

    // Create a Player
    Player player("Adventurer", 100);
    // Set the player's starting location
    player.SetLocation(&startRoom);

    bool bossFight = false; // Declare and initialize bossFight variable
    bool itemFound = false;
    int bossesDefeated = 0; // Initialize the count of defeated bosses

    // Game loop (basic interaction)
    while (bossesDefeated < 5) { // Continue until all bosses are defeated
        while (true) {
            std::cout << "Current Location: " << player.GetLocation()->GetDescription(), std::cout << "                                           Player Health: " << player.GetHealth() << std::endl;
            // Display player's inventory
            player.GetInventory().DisplayItems();
            std::cout << "Items in the room:" << std::endl;
            for (const Item& item : player.GetLocation()->GetItems()) {
                std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
            }

            // Check if the room has a boss
            Boss* boss = player.GetLocation()->GetBoss();
            if (boss != nullptr) {
                std::cout << "A fearsome " << boss->GetName() << " lurks in this room!" << std::endl;
                std::cout << "Options: ";
                std::cout << "1. Look around | ";
                std::cout << "2. Interact with an item | ";
                std::cout << "3. Move to another room | ";
                std::cout << "4. Fight the boss | ";
                std::cout << "5. Quit" << std::endl;

                int choice;
                std::cin >> choice;

                // Clear input buffer
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

                std::string itemName; // Declare itemName outside the switch statement
                std::string direction; // Declare direction outside the switch statement
                Room* nextRoom = nullptr; // Declare nextRoom outside the switch block
                std::map<std::string, Room*> exits; // Declare exits before the switch statement
                std::vector<std::string> availableExits; // Declare availableExits outside the switch statement
                std::vector<std::string> exitDirections; // Declare the variable outside the switch statement

                switch (choice) {
                case 1:
                    // Player looks around (no action required)
                    std::cout << "You look around the room." << std::endl;
                    break;
                case 2:
                {
                    // Player interacts with an item in the room
                    std::cout << "Enter the name of the item you want to interact with: ";
                    std::string itemName;
                    std::cin >> itemName;
                    bool itemFound = false;

                    // Check if the item exists in the player's inventory
                    if (player.GetInventory().HasItem(itemName)) {
                        // If the item is the Juggernog, use it to increase player's health
                        if (itemName == "Juggernog") {
                            player.IncreaseHealth(100); // Increase player's health by 100
                            std::cout << "You consumed the Juggernog. Your health is increased by 100." << std::endl;
                            player.GetInventory().RemoveItem(itemName); // Remove the Juggernog from the player's inventory
                            itemFound = true;
                        }
                        else {
                            std::cout << "You cannot interact with this item." << std::endl;
                        }
                    }
                    else {
                        // Check if the item exists in the current room
                        for (auto it = player.GetLocation()->GetItems().begin(); it != player.GetLocation()->GetItems().end(); ++it) {
                            if (it->GetName() == itemName) {
                                const Item& itemToInteract = *it; // Dereference the iterator to get a reference to the item
                                std::cout << "You interact with the item: " << itemToInteract.GetName() << std::endl;
                                itemFound = true;

                                if (itemName == "Key" && player.GetLocation() == &startRoom) {
                                    // Player can pick up the key only in the starting room
                                    std::cout << "You picked up the key and added it to your inventory." << std::endl;
                                    player.GetInventory().AddItem(*it); // Add the key to the player's inventory
                                    player.GetLocation()->RemoveItem(itemName); // Remove the key from the room
                                }
                                else if (itemName == "Chest" && player.GetInventory().HasItem("Key") && player.GetLocation() == &northwestRoom) {
                                    // Player can open the chest if they have the key and are in the northwest room
                                    std::cout << "You use the key to unlock the chest." << std::endl;
                                    player.GetInventory().RemoveItem("Key"); // Remove the key from the player's inventory
                                    player.GetInventory().AddItem(juggernog); // Add the Juggernog to the player's inventory
                                    std::cout << "You obtained JUGGERNOG! Your current health is increased by 100." << std::endl;
                                    player.GetLocation()->RemoveItem(itemName); // Remove the chest from the room
                                }
                                else if (itemName == "Sword" && player.GetLocation() == &southeastRoom) {
                                    // Player can pick up the sword only in the southeast room
                                    std::cout << "You picked up the sword and added it to your inventory." << std::endl;
                                    player.GetInventory().AddItem(sword); // Add the key to the player's inventory
                                    player.GetLocation()->RemoveItem(itemName); // Remove the key from the room
                                }
                                else {
                                    std::cout << "You cannot interact with this item." << std::endl;
                                }

                                break; // Exit the loop once the item is found and interacted with
                            }
                        }
                    }

                    if (!itemFound) {
                        std::cout << "No such item found." << std::endl;
                    }

                    break;
                }
                case 3:
                    // Player moves to another room
                    std::cout << "Available exits: ";
                    exitDirections = player.GetLocation()->GetExits(); // Initialize the variable within the case block
                    for (const std::string& direction : exitDirections) {
                        std::cout << direction << ", ";
                    }
                    std::cout << std::endl;

                    std::cout << "Enter the direction to move: ";
                    std::cin >> direction;
                    nextRoom = player.GetLocation()->GetExit(direction);
                    if (nextRoom != nullptr) {
                        player.SetLocation(nextRoom);
                        std::cout << "You move to the " << nextRoom->GetDescription() << "." << std::endl;

                        // Update available exits
                        exits.clear();
                        for (const std::string& dir : player.GetLocation()->GetExits()) {
                            exits[dir] = player.GetLocation()->GetExit(dir);
                        }
                    }
                    else {
                        std::cout << "Invalid direction. You cannot go that way." << std::endl;
                    }
                    break;
                case 4:
                {
                    int playerDamage = 0;
                    int bossDamage = 0;

                    bool bossFight = true; // Set bossFight to true to enter the fight

                    while (bossFight) {
                        // Check if the room has a boss
                        std::cout << "Player Health: " << player.GetHealth() << std::endl;
                        std::cout << "Boss Health: " << boss->GetHealth() << std::endl;
                        Boss* boss = player.GetLocation()->GetBoss();
                        if (boss != nullptr) {
                            std::cout << "A fearsome " << boss->GetName() << " lurks in this room!" << std::endl;
                            std::cout << "Choose your action: " << std::endl;
                            std::cout << "1. Attack the boss" << std::endl;
                            std::cout << "2. Attempt to dodge" << std::endl;
                            std::cout << "3. Attempt to block" << std::endl;
                            std::cout << "4. Attempt to run away" << std::endl;

                            int actionChoice;
                            std::cin >> actionChoice;

                            // Clear input buffer
                            std::cin.clear();
                            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

                            switch (actionChoice) {
                            case 1:
                                // Player attacks boss
                                if (player.HasItem("Sword")) { // Check if the player has the sword
                                    playerDamage = 80; // If player has the sword, set damage to 80
                                    std::cout << "You attack the " << boss->GetName() << " with the Sword for " << playerDamage << " damage!" << std::endl;
                                }
                                else {
                                    playerDamage = 30; // Base player damage without the sword
                                    std::cout << "You attack the " << boss->GetName() << " for " << playerDamage << " damage!" << std::endl;
                                }
                                boss->TakeDamage(playerDamage);

                                // Boss retaliates
                                bossDamage = boss->GetDamage(); // Base boss damage
                                player.TakeDamage(bossDamage);
                                std::cout << "The " << boss->GetName() << " attacks you for " << bossDamage << " damage!" << std::endl;
                                break;

                            case 2:
                                // Player attempts to dodge (60% chance)
                                if (rand() % 100 < 60) { // 60% chance to dodge
                                    std::cout << "You dodge the " << boss->GetName() << "'s attack!" << std::endl;
                                }
                                else {
                                    bossDamage = static_cast<int>(boss->GetDamage() * 1.5); // Base boss damage
                                    player.TakeDamage(bossDamage);
                                    std::cout << "You failed to dodge and take " << bossDamage << " damage!" << std::endl;
                                }
                                break;

                            case 3:
                                // Player attempts to block (reduces boss damage by 30%)
                                bossDamage = boss->GetDamage(); // Base boss damage
                                bossDamage = static_cast<int>(bossDamage * 0.7); // Reduce boss damage by 30%
                                player.TakeDamage(bossDamage);
                                std::cout << "You block the " << boss->GetName() << "'s attack and take reduced damage!" << std::endl;
                                break;

                            case 4:
                                // Player attempts to run away (lose 50% of health)
                                player.TakeDamage(player.GetHealth() / 2); // Lose 50% of health
                                std::cout << "You attempt to run away but the " << boss->GetName() << " catches you and inflicts" << boss->GetDamage() * 0.5 << " damage!" << std::endl;
                                bossFight = false; // Exit the boss fight loop
                                break;

                            default:
                                std::cout << "Invalid choice. Try again." << std::endl;
                                break;
                            }

                            if (boss->GetHealth() <= 0) {
                                std::cout << "You defeated the " << boss->GetName() << "!" << std::endl;
                                player.GetLocation()->DefeatBoss(); // Remove the boss from the room
                                bossFight = false; // Exit the boss fight loop
                                bossesDefeated++; // Increment the count of defeated bosses
                                std::cout << "BOSSES DEFEATED: " << bossesDefeated << std::endl;//testing
                                break; // Exit the inner loop
                            }

                            if (player.GetHealth() <= 0) {
                                std::cout << "You were defeated by the " << boss->GetName() << "!" << std::endl;
                                bossFight = false; // Exit the boss fight loop
                                break; // Exit the inner loop
                            }
                        }
                    }
                    break;
                }
                case 5:
                    // Quit the game
                    std::cout << "Goodbye!" << std::endl;
                    return 0;
                default:
                    std::cout << "Invalid choice. Try again." << std::endl;
                    break;
                }
            }
            else {
                std::cout << "Options: ";
                std::cout << "1. Look around | ";
                std::cout << "2. Interact with an item | ";
                std::cout << "3. Move to another room | ";
                std::cout << "4. Quit" << std::endl;

                int choice;
                std::cin >> choice;

                // Clear input buffer
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

                std::string itemName; // Declare itemName outside the switch statement
                std::string direction; // Declare direction outside the switch statement
                Room* nextRoom = nullptr; // Declare nextRoom outside the switch block
                std::map<std::string, Room*> exits; // Declare exits before the switch statement
                std::vector<std::string> availableExits; // Declare availableExits outside the switch statement
                std::vector<std::string> exitDirections; // Declare the variable outside the switch statement

                switch (choice) {
                case 1:
                    // Player looks around (no action required)
                    std::cout << "You look around the room." << std::endl;
                    break;
                case 2:
                {
                    // Player interacts with an item in the room
                    std::cout << "Enter the name of the item you want to interact with: ";
                    std::string itemName;
                    std::cin >> itemName;
                    bool itemFound = false;

                    // Check if the item exists in the player's inventory
                    if (player.GetInventory().HasItem(itemName)) {
                        // If the item is the Juggernog, use it to increase player's health
                        if (itemName == "Juggernog") {
                            player.IncreaseHealth(100); // Increase player's health by 100
                            std::cout << "You consumed the Juggernog. Your health is increased by 100." << std::endl;
                            player.GetInventory().RemoveItem(itemName); // Remove the Juggernog from the player's inventory
                            itemFound = true;
                        }
                        else {
                            std::cout << "You cannot interact with this item." << std::endl;
                        }
                    }
                    else {
                        // Check if the item exists in the current room
                        for (auto it = player.GetLocation()->GetItems().begin(); it != player.GetLocation()->GetItems().end(); ++it) {
                            if (it->GetName() == itemName) {
                                const Item& itemToInteract = *it; // Dereference the iterator to get a reference to the item
                                std::cout << "You interact with the item: " << itemToInteract.GetName() << std::endl;
                                itemFound = true;

                                if (itemName == "Key" && player.GetLocation() == &startRoom) {
                                    // Player can pick up the key only in the starting room
                                    std::cout << "You picked up the key and added it to your inventory." << std::endl;
                                    player.GetInventory().AddItem(*it); // Add the key to the player's inventory
                                    player.GetLocation()->RemoveItem(itemName); // Remove the key from the room
                                }
                                else if (itemName == "Chest" && player.GetInventory().HasItem("Key") && player.GetLocation() == &northwestRoom) {
                                    // Player can open the chest if they have the key and are in the northwest room
                                    std::cout << "You use the key to unlock the chest." << std::endl;
                                    player.GetInventory().RemoveItem("Key"); // Remove the key from the player's inventory
                                    player.GetInventory().AddItem(juggernog); // Add the Juggernog to the player's inventory
                                    std::cout << "You obtained JUGGERNOG! Your current health is increased by 100." << std::endl;
                                    player.GetLocation()->RemoveItem(itemName); // Remove the chest from the room
                                }
                                else if (itemName == "Sword" && player.GetLocation() == &southeastRoom) {
                                    // Player can pick up the sword only in the southeast room
                                    std::cout << "You picked up the sword and added it to your inventory." << std::endl;
                                    player.GetInventory().AddItem(sword); // Add the key to the player's inventory
                                    player.GetLocation()->RemoveItem(itemName); // Remove the key from the room
                                }
                                else {
                                    std::cout << "You cannot interact with this item." << std::endl;
                                }

                                break; // Exit the loop once the item is found and interacted with
                            }
                        }
                    }

                    if (!itemFound) {
                        std::cout << "No such item found." << std::endl;
                    }

                    break;
                }
                case 3:
                    // Player moves to another room
                    std::cout << "Available exits: ";
                    exitDirections = player.GetLocation()->GetExits(); // Initialize the variable within the case block
                    for (const std::string& direction : exitDirections) {
                        std::cout << direction << ", ";
                    }
                    std::cout << std::endl;

                    std::cout << "Enter the direction to move: ";
                    std::cin >> direction;
                    nextRoom = player.GetLocation()->GetExit(direction);
                    if (nextRoom != nullptr) {
                        player.SetLocation(nextRoom);
                        std::cout << "You move to the " << nextRoom->GetDescription() << "." << std::endl;

                        // Update available exits
                        exits.clear();
                        for (const std::string& dir : player.GetLocation()->GetExits()) {
                            exits[dir] = player.GetLocation()->GetExit(dir);
                        }
                    }
                    else {
                        std::cout << "Invalid direction. You cannot go that way." << std::endl;
                    }
                    break;
                case 4:
                    // Quit the game
                    std::cout << "Goodbye!" << std::endl;
                    return 0;
                default:
                    std::cout << "Invalid choice. Try again." << std::endl;
                    break;
                }
            }
            // Check if all bosses are defeated
                if (bossesDefeated == 5) {
                break; // Exit the outer loop if all bosses are defeated
            }
        }




    }
    // Display ending message
    std::cout << "Congratulations! You have defeated all five bosses and emerged victorious!" << std::endl;
    std::cout << "Thank you for playing!" << std::endl;

    return 0;
}
