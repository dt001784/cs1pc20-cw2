#// Boss.h

#ifndef BOSS_H
#define BOSS_H

#include <iostream>
#include <string>
#include "Item.h"
#include <vector>

class Boss {
private:
    std::string name;
    int health;
    int damage;

public:
    Boss(const std::string& name, int health, int damage);
    std::string GetName() const;
    int GetHealth() const;
    int GetDamage() const;
    void TakeDamage(int damage); // Declaration of TakeDamage
};

#endif // BOSS_H
