// Character.h

#ifndef CHARACTER_H
#define CHARACTER_H

#include <iostream>
#include <string>
#include <vector>
#include "Item.h"
#include "inventory.h"


class Character {
private:
    std::string name;
    std::vector<Item> inventory;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    int health;
};

#endif // CHARACTER_H