// Room.cpp

#include "Room.h"
#include <algorithm>

Room::Room(const std::string& desc) : description(desc), boss(nullptr) {} // Initialize boss to nullptr

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

void Room::RemoveItem(const std::string& itemName) {
    auto it = std::find_if(items.begin(), items.end(), [&](const Item& item) {
        return item.GetName() == itemName;
        });

    if (it != items.end()) {
        items.erase(it);
    }
}


std::vector<Item>& Room::GetItems() {
    return items;
}

Room* Room::GetExit(const std::string& direction) {
    if (exits.find(direction) != exits.end()) {
        return exits[direction];
    }
    return nullptr;
}

Boss* Room::GetBoss() {
    return boss;
}

void Room::SetBoss(Boss* newBoss) {
    boss = newBoss;
}

std::vector<std::string> Room::GetExits() const {
    std::vector<std::string> exitList;
    for (const auto& exit : exits) {
        exitList.push_back(exit.first);
    }
    return exitList;
}

std::vector<std::string> Room::GetAvailableDirections() const {
    std::vector<std::string> availableDirections;
    for (const auto& exit : exits) {
        availableDirections.push_back(exit.first);
    }
    return availableDirections;
}