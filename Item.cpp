// In Item.cpp

#include "Item.h"
#include "Player.h"

Item::Item(const std::string& name, const std::string& description)
    : name(name), description(description) {}


// Definition of the equality operator
bool Item::operator==(const Item& other) const {
    return name == other.name && description == other.description;
}

const std::string Item::GetName() const {
    return name;
}

const std::string Item::GetDescription() const {
    return description;
}

void Item::Interact() {
    std::cout << "You interact with the item: " << name << std::endl;
}