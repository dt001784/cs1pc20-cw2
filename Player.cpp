// Player.cpp

#include "Player.h"
#include "Boss.h"

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

void Player::SetLocation(Room* room) {
    location = room;
}

Room* Player::GetLocation() const {
    return location;
}

Inventory& Player::GetInventory() {
    return inventory;
}

int Player::GetHealth() const {
    return health; // Assuming 'health' is a member variable representing the player's health
}

bool Player::HasItem(const std::string& itemName) const {
    return inventory.HasItem(itemName); // Delegate to the Inventory class method
}

void Player::IncreaseHealth(int amount) {
    health += amount; // Increase player's health by the specified amount
}

void Player::TakeDamage(int damage) {
    health -= damage;
}

void Player::UseItem(const std::string& itemName, Boss& boss) {
    // Implement item usage logic here
    // For example, find the item in the player's inventory and apply its effect on the boss
}