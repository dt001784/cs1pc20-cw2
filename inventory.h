#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Item.h"

class Inventory {
private:
    std::vector<Item> items;

public:
    void AddItem(const Item& item);
    void DisplayItems();
    const std::vector<Item>& GetItems() const;
    bool RemoveItem(const std::string& name);
    bool HasItem(const std::string& itemName) const; // Declaration of HasItem method

};