#include "inventory.h"

void Inventory::AddItem(const Item& item) {
    items.push_back(item);
}

void Inventory::DisplayItems() {
    std::cout << "Inventory contains:" << std::endl;
    for (const Item& item : items) {
        std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
    }
}

const std::vector<Item>& Inventory::GetItems() const {
    return items;
}

bool Inventory::RemoveItem(const std::string& name) {
    for (size_t i = 0; i < items.size(); i++) {
        if (items[i].GetName() == name) {
            items.erase(items.begin() + i);
            return true;
        }
    }
    return false;
}

bool Inventory::HasItem(const std::string& itemName) const {
    for (const Item& item : items) {
        if (item.GetName() == itemName) {
            return true;
        }
    }
    return false;
}