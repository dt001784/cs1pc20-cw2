// Boss.cpp

#include "Boss.h"

Boss::Boss(const std::string& name, int health, int damage) : name(name), health(health), damage(damage) {}

std::string Boss::GetName() const {
    return name;
}

int Boss::GetHealth() const {
    return health;
}

int Boss::GetDamage() const {
    return damage;
}

void Boss::TakeDamage(int damage) {
    health -= damage;
}
