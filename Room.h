#ifndef ROOM_H
#define ROOM_H

#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "Item.h"
#include "Boss.h" // Include the Boss class header

extern bool bossPresent; // Declare bossPresent as extern

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;
    Boss* boss; // Member for Boss

public:
    Room(const std::string& desc);
    void AddItem(const Item& item);
    void AddExit(const std::string& direction, Room* room);
    void RemoveItem(const std::string& itemName);
    std::string GetDescription() const {
        std::string roomDescription = description;
        return roomDescription;
    }
    std::vector<Item>& GetItems();
    Room* GetExit(const std::string& direction);
    Boss* GetBoss(); // Method to get the boss
    void SetBoss(Boss* boss); // Method to set the boss
    std::vector<std::string> GetExits() const; // Declaration only
    std::vector<std::string> GetAvailableDirections() const; // Declaration only
    // Method to check if boss is present
    bool IsBossPresent() const {
        return bossPresent;
    }

    // Method to remove boss from the room
    void DefeatBoss() {
        boss = nullptr;
        bossPresent = false;
    }
};

#endif // ROOM_H
